# Ansible WireGuard Role

An Ansible role to configure [WireGuard](https://www.wireguard.com/) on Debian
and Ubuntu servers based on [Ubuntu 20.04 set up WireGuard VPN
server](https://www.cyberciti.biz/faq/ubuntu-20-04-set-up-wireguard-vpn-server/)
and [How To Set Up WireGuard Firewall Rules in
Linux](https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/),
see also the [Quick Start
documentation](https://www.wireguard.com/quickstart/).


This role is designed to be used with an inventory / variables like this, you
need to provide values for the `public_key` variables from the clients and pick
a port number:

```yml
all:
  children:
    wireguard_servers:
      hosts:
        wg.example.org:
          vars:
            wireguard_devices:
              wg0:
                interface:
                  ipv4_address: 192.168.8.1
                  ipv4_subnet: 255.255.255.0
                  listen_port: 50000
                peers:
                  - name: Desktop
                    public_key: 123ABC
                    allowed_ips:
                      - 192.168.8.2
                  - name: Laptop
                    public_key: FOOBAR
                    allowed_ips:
                      - 192.168.8.3
```

The server private key is automatically generated and included in the
configuration file, for the client install WireGuard:

```bash
sudo apt update
sudo apt install wireguard -y
```

Generate a private and public key:

```bash
sudo -i
cd /etc/wireguard/
umask 077; wg genkey | tee privatekey | wg pubkey > publickey
cat publickey
```

The `publickey` contents needs to be added to Ansible to add it to the server,
the `privatekey`, needs to be added to a file you need to create, for example
`/etc/wireguard/wg0.conf`, it should be owned and only readable by `root` (mode
`0600` and `root:root`).

The `/etc/wireguard/wg0.conf` client configuration files can be based on the
following example, the two instances of `XXX`, the IP addres and port,
`PUBLIC_IPV4_ADDRESS:50000` and probably the IP `Address` value need to be
replaced:

```conf
# WireGuard Client Configuration

[Interface]
# The contents of /etc/wireguard/privatekey:
PrivateKey = XXX

# The private IP address
Address = 192.168.8.2/24

[Peer]
# The server public key, address and port
PublicKey = XXX
AllowedIPs = 0.0.0.0/0
Endpoint = PUBLIC_IPV4_ADDRESS:50000
PersistentKeepalive = 15
```

You can then start the VPN on the client:

```bash
sudo wg-quick up wg0
sudo wg show
```

To stop the VPN:

```bash
sudo wg-quick down wg0
```

## TODO

1. Add support for client configuration for clients that are reachable via a public IP address.

